# Catalan Airlines.
## Fake Flights Company: Bookings Page.

This repository includes:

- Source Code (WebPage - Using MeteorJS, Handlebars & BootstrapCSS)
- Manual Page (DocBook & Markdown versions)
- CatalanAirlines Search Engine (Using ElasticSearch)
